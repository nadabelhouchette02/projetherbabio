import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './home/accueil/accueil.component';
import { AvantageComponent } from './home/avantage/avantage.component';
import { ConseilComponent } from './home/conseil/conseil.component';
import { ContactComponent } from './home/contact/contact.component';
import { ProduitComponent } from './home/produit/produit.component';

const routes: Routes = [
  {path : "" , component : AccueilComponent},
  {path : "accueil" , component : AccueilComponent},
  {path : "produit" , component : ProduitComponent},
  {path : "conseil" , component : ConseilComponent},
  {path : "avantage" , component : AvantageComponent},
  {path : "contact" , component : ContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
