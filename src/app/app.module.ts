import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { AccueilComponent } from './home/accueil/accueil.component';
import { ProduitComponent } from './home/produit/produit.component';
import { ConseilComponent } from './home/conseil/conseil.component';
import { AvantageComponent } from './home/avantage/avantage.component';
import { ContactComponent } from './home/contact/contact.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AccueilComponent,
    ProduitComponent,
    ConseilComponent,
    AvantageComponent,
    ContactComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
 
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
