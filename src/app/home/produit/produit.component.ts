import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.css']
})
export class ProduitComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


   
produits= [
  {pro: 1, name:'Naturex creme de change 50g',srcLink:'assets/creme.webp'},
  {pro: 2, name:'Naturex eau de toilette garcon 150ml',srcLink:'assets/garcon.jfif'},
  {pro: 3, name:'Naturex eau de toilette filles 150ml ',srcLink:'assets/fille.jfif'},
  {pro: 4, name:'naturex lait bebe 100ml',srcLink:'assets/lait.jfif'},
  {pro: 5, name:'naturex shampoing bebe 250ml ',srcLink:'assets/champoing.webp'},
  {pro: 6, name:'Morinex repousse 50ml',srcLink:'assets/repousse.webp'},
  {pro: 7, name:'Naturex creme hydratante 50g',srcLink:'assets/cremeH.webp'},
  {pro: 8, name:'Morinex all in one 50ml',srcLink:'assets/all.webp'},
  {pro: 9, name:'Morinex vitE 30ml',srcLink:'assets/vitE.webp'},
];

}
